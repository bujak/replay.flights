replay.flights
====================
[https://replay.flights](https://replay.flights) is a web based flights visualisation in an animated 3D map. Just drag & drop your GPS track files to review them. Quick start guide: https://gitlab.com/bujak/replay.flights/-/wikis/Start-using-replay.flights

Support
=======
Your donation to cover operating costs will be greatly appreciated 
https://paypal.me/replayflights

Bugs
====
If you find a bug or have an idea for a new feature, I would be happy if you contact me at <a href="mailto:info@replay.flights">info@replay.flights</a> or even better - check the <a href="https://gitlab.com/bujak/replay.flights/-/issues">issue tracker</a> and if it's not mentioned there, directly <a href="https://gitlab.com/bujak/replay.flights/-/issues/new">create a new Gitlab issue</a> (you will need a Gitlab account to do that). If applicable, attach the problematic IGC / GPX / CSV / airspace / task files. 

Contact
=======
info[at]replay.flights
